﻿namespace MiniSniffer
{
    partial class FilterOption
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox_Protocol = new System.Windows.Forms.GroupBox();
            this.CheckBox_IGMP = new System.Windows.Forms.CheckBox();
            this.CheckBox_ICMP = new System.Windows.Forms.CheckBox();
            this.CheckBox_UDP = new System.Windows.Forms.CheckBox();
            this.CheckBox_TCP = new System.Windows.Forms.CheckBox();
            this.GroupBox_Source = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_SourcePort = new System.Windows.Forms.TextBox();
            this.TextBox_SourceIP = new System.Windows.Forms.TextBox();
            this.GroupBox_Dest = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBox_DestPort = new System.Windows.Forms.TextBox();
            this.TextBox_DestIP = new System.Windows.Forms.TextBox();
            this.Button_OK = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.GroupBox_Protocol.SuspendLayout();
            this.GroupBox_Source.SuspendLayout();
            this.GroupBox_Dest.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox_Protocol
            // 
            this.GroupBox_Protocol.Controls.Add(this.CheckBox_IGMP);
            this.GroupBox_Protocol.Controls.Add(this.CheckBox_ICMP);
            this.GroupBox_Protocol.Controls.Add(this.CheckBox_UDP);
            this.GroupBox_Protocol.Controls.Add(this.CheckBox_TCP);
            this.GroupBox_Protocol.Location = new System.Drawing.Point(12, 12);
            this.GroupBox_Protocol.Name = "GroupBox_Protocol";
            this.GroupBox_Protocol.Size = new System.Drawing.Size(309, 49);
            this.GroupBox_Protocol.TabIndex = 1;
            this.GroupBox_Protocol.TabStop = false;
            this.GroupBox_Protocol.Text = "协议";
            // 
            // CheckBox_IGMP
            // 
            this.CheckBox_IGMP.AutoSize = true;
            this.CheckBox_IGMP.Checked = true;
            this.CheckBox_IGMP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_IGMP.Location = new System.Drawing.Point(222, 19);
            this.CheckBox_IGMP.Name = "CheckBox_IGMP";
            this.CheckBox_IGMP.Size = new System.Drawing.Size(53, 17);
            this.CheckBox_IGMP.TabIndex = 4;
            this.CheckBox_IGMP.Text = "IGMP";
            this.CheckBox_IGMP.UseVisualStyleBackColor = true;
            // 
            // CheckBox_ICMP
            // 
            this.CheckBox_ICMP.AutoSize = true;
            this.CheckBox_ICMP.Checked = true;
            this.CheckBox_ICMP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_ICMP.Location = new System.Drawing.Point(154, 19);
            this.CheckBox_ICMP.Name = "CheckBox_ICMP";
            this.CheckBox_ICMP.Size = new System.Drawing.Size(52, 17);
            this.CheckBox_ICMP.TabIndex = 3;
            this.CheckBox_ICMP.Text = "ICMP";
            this.CheckBox_ICMP.UseVisualStyleBackColor = true;
            // 
            // CheckBox_UDP
            // 
            this.CheckBox_UDP.AutoSize = true;
            this.CheckBox_UDP.Checked = true;
            this.CheckBox_UDP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_UDP.Location = new System.Drawing.Point(89, 19);
            this.CheckBox_UDP.Name = "CheckBox_UDP";
            this.CheckBox_UDP.Size = new System.Drawing.Size(49, 17);
            this.CheckBox_UDP.TabIndex = 2;
            this.CheckBox_UDP.Text = "UDP";
            this.CheckBox_UDP.UseVisualStyleBackColor = true;
            // 
            // CheckBox_TCP
            // 
            this.CheckBox_TCP.AutoSize = true;
            this.CheckBox_TCP.Checked = true;
            this.CheckBox_TCP.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBox_TCP.Location = new System.Drawing.Point(26, 19);
            this.CheckBox_TCP.Name = "CheckBox_TCP";
            this.CheckBox_TCP.Size = new System.Drawing.Size(47, 17);
            this.CheckBox_TCP.TabIndex = 1;
            this.CheckBox_TCP.Text = "TCP";
            this.CheckBox_TCP.UseVisualStyleBackColor = true;
            // 
            // GroupBox_Source
            // 
            this.GroupBox_Source.Controls.Add(this.label2);
            this.GroupBox_Source.Controls.Add(this.label1);
            this.GroupBox_Source.Controls.Add(this.TextBox_SourcePort);
            this.GroupBox_Source.Controls.Add(this.TextBox_SourceIP);
            this.GroupBox_Source.Location = new System.Drawing.Point(12, 67);
            this.GroupBox_Source.Name = "GroupBox_Source";
            this.GroupBox_Source.Size = new System.Drawing.Size(309, 58);
            this.GroupBox_Source.TabIndex = 4;
            this.GroupBox_Source.TabStop = false;
            this.GroupBox_Source.Text = "源";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(186, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "端口:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "IP:";
            // 
            // TextBox_SourcePort
            // 
            this.TextBox_SourcePort.Location = new System.Drawing.Point(222, 19);
            this.TextBox_SourcePort.Name = "TextBox_SourcePort";
            this.TextBox_SourcePort.Size = new System.Drawing.Size(64, 20);
            this.TextBox_SourcePort.TabIndex = 5;
            // 
            // TextBox_SourceIP
            // 
            this.TextBox_SourceIP.Location = new System.Drawing.Point(49, 19);
            this.TextBox_SourceIP.Name = "TextBox_SourceIP";
            this.TextBox_SourceIP.Size = new System.Drawing.Size(117, 20);
            this.TextBox_SourceIP.TabIndex = 4;
            // 
            // GroupBox_Dest
            // 
            this.GroupBox_Dest.Controls.Add(this.label3);
            this.GroupBox_Dest.Controls.Add(this.label4);
            this.GroupBox_Dest.Controls.Add(this.TextBox_DestPort);
            this.GroupBox_Dest.Controls.Add(this.TextBox_DestIP);
            this.GroupBox_Dest.Location = new System.Drawing.Point(12, 131);
            this.GroupBox_Dest.Name = "GroupBox_Dest";
            this.GroupBox_Dest.Size = new System.Drawing.Size(309, 58);
            this.GroupBox_Dest.TabIndex = 8;
            this.GroupBox_Dest.TabStop = false;
            this.GroupBox_Dest.Text = "目标";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(186, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "端口:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "IP:";
            // 
            // TextBox_DestPort
            // 
            this.TextBox_DestPort.Location = new System.Drawing.Point(222, 19);
            this.TextBox_DestPort.Name = "TextBox_DestPort";
            this.TextBox_DestPort.Size = new System.Drawing.Size(64, 20);
            this.TextBox_DestPort.TabIndex = 5;
            // 
            // TextBox_DestIP
            // 
            this.TextBox_DestIP.Location = new System.Drawing.Point(49, 19);
            this.TextBox_DestIP.Name = "TextBox_DestIP";
            this.TextBox_DestIP.Size = new System.Drawing.Size(117, 20);
            this.TextBox_DestIP.TabIndex = 4;
            // 
            // Button_OK
            // 
            this.Button_OK.Location = new System.Drawing.Point(165, 204);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 9;
            this.Button_OK.Text = "确定";
            this.Button_OK.UseVisualStyleBackColor = true;
            this.Button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(246, 204);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "取消";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // FilterOption
            // 
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(333, 239);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.GroupBox_Dest);
            this.Controls.Add(this.GroupBox_Source);
            this.Controls.Add(this.GroupBox_Protocol);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FilterOption";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "筛选选项";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FilterOption_FormClosing);
            this.GroupBox_Protocol.ResumeLayout(false);
            this.GroupBox_Protocol.PerformLayout();
            this.GroupBox_Source.ResumeLayout(false);
            this.GroupBox_Source.PerformLayout();
            this.GroupBox_Dest.ResumeLayout(false);
            this.GroupBox_Dest.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBox_Protocol;
        private System.Windows.Forms.CheckBox CheckBox_TCP;
        private System.Windows.Forms.CheckBox CheckBox_IGMP;
        private System.Windows.Forms.CheckBox CheckBox_ICMP;
        private System.Windows.Forms.CheckBox CheckBox_UDP;
        private System.Windows.Forms.GroupBox GroupBox_Source;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_SourcePort;
        private System.Windows.Forms.TextBox TextBox_SourceIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox GroupBox_Dest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBox_DestPort;
        private System.Windows.Forms.TextBox TextBox_DestIP;
        private System.Windows.Forms.Button Button_OK;
        private System.Windows.Forms.Button Button_Cancel;
    }
}