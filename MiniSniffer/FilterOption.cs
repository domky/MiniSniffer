using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MiniSniffer
{
    public partial class FilterOption : Form
    {
        public string Protocol;
        public string SourceIP;
        public string SourcePort;
        public string DestIP;
        public string DestPort;

        public FilterOption()
        {
            Protocol = "TCP,UDP,ICMP,IGMP,";
            SourceIP = "";
            SourcePort = "";
            DestIP = "";
            DestPort = "";
            InitializeComponent();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            SaveSettings();
            this.Hide();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            LoadSettings();
            this.Hide();
        }

        private void FilterOption_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            LoadSettings();
            this.Hide();
        }

        private void LoadSettings()
        {
            if (Protocol.IndexOf("TCP") != -1) CheckBox_TCP.Checked = true; else CheckBox_TCP.Checked = false;
            if (Protocol.IndexOf("UDP") != -1) CheckBox_UDP.Checked = true; else CheckBox_UDP.Checked = false;
            if (Protocol.IndexOf("ICMP") != -1) CheckBox_ICMP.Checked = true; else CheckBox_ICMP.Checked = false;
            if (Protocol.IndexOf("IGMP") != -1) CheckBox_IGMP.Checked = true; else CheckBox_IGMP.Checked = false;

            TextBox_SourceIP.Text = SourceIP;
            TextBox_SourcePort.Text = SourcePort;
            TextBox_DestIP.Text = DestIP;
            TextBox_DestPort.Text = DestPort;
        }

        private void SaveSettings()
        {
            string ProtocolString = "";
            if (CheckBox_TCP.Checked) ProtocolString += "TCP,";
            if (CheckBox_UDP.Checked) ProtocolString += "UDP,";
            if (CheckBox_ICMP.Checked) ProtocolString += "ICMP,";
            if (CheckBox_IGMP.Checked) ProtocolString += "IGMP,";
            Protocol = ProtocolString;

            SourceIP = TextBox_SourceIP.Text;
            SourcePort = TextBox_SourcePort.Text;
            DestIP = TextBox_DestIP.Text;
            DestPort = TextBox_DestPort.Text;
        }
    }
}