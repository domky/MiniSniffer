﻿namespace MiniSniffer
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.监视ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.启动ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.停止ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.清空ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.筛选ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.筛选选项ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStrip_Open = new System.Windows.Forms.ToolStripButton();
            this.ToolStrip_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStrip_Start = new System.Windows.Forms.ToolStripButton();
            this.ToolStrip_Stop = new System.Windows.Forms.ToolStripButton();
            this.ToolStrip_Clear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStrip_Filter = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listView_Data = new System.Windows.Forms.ListView();
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Protocol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SourceIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SourcePort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DestIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DestPort = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MessageBodyLen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MessageBodyTxt = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IPHeaderHex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MessageHeaderHex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MessageBodyHex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IPHeaderLength= ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MessageHeaderLen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.TextBox_Txt = new System.Windows.Forms.TextBox();
            this.TextBox_Hex = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ServiceStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.PacketStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件FToolStripMenuItem,
            this.监视ToolStripMenuItem,
            this.筛选ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(804, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件FToolStripMenuItem
            // 
            this.文件FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开ToolStripMenuItem,
            this.保存ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.退出ToolStripMenuItem});
            this.文件FToolStripMenuItem.Name = "文件FToolStripMenuItem";
            this.文件FToolStripMenuItem.Size = new System.Drawing.Size(58, 21);
            this.文件FToolStripMenuItem.Text = "文件(&F)";
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.打开ToolStripMenuItem.Text = "打开";
            this.打开ToolStripMenuItem.Click += new System.EventHandler(this.打开ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.保存ToolStripMenuItem.Text = "保存";
            this.保存ToolStripMenuItem.Click += new System.EventHandler(this.保存ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(97, 6);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 监视ToolStripMenuItem
            // 
            this.监视ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.启动ToolStripMenuItem,
            this.停止ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.清空ToolStripMenuItem});
            this.监视ToolStripMenuItem.Name = "监视ToolStripMenuItem";
            this.监视ToolStripMenuItem.Size = new System.Drawing.Size(59, 21);
            this.监视ToolStripMenuItem.Text = "监听(&S)";
            // 
            // 启动ToolStripMenuItem
            // 
            this.启动ToolStripMenuItem.Name = "启动ToolStripMenuItem";
            this.启动ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.启动ToolStripMenuItem.Text = "启动";
            this.启动ToolStripMenuItem.Click += new System.EventHandler(this.启动ToolStripMenuItem_Click);
            // 
            // 停止ToolStripMenuItem
            // 
            this.停止ToolStripMenuItem.Name = "停止ToolStripMenuItem";
            this.停止ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.停止ToolStripMenuItem.Text = "停止";
            this.停止ToolStripMenuItem.Click += new System.EventHandler(this.停止ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(97, 6);
            // 
            // 清空ToolStripMenuItem
            // 
            this.清空ToolStripMenuItem.Name = "清空ToolStripMenuItem";
            this.清空ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.清空ToolStripMenuItem.Text = "清空";
            this.清空ToolStripMenuItem.Click += new System.EventHandler(this.清空ToolStripMenuItem_Click);
            // 
            // 筛选ToolStripMenuItem
            // 
            this.筛选ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.筛选选项ToolStripMenuItem});
            this.筛选ToolStripMenuItem.Name = "筛选ToolStripMenuItem";
            this.筛选ToolStripMenuItem.Size = new System.Drawing.Size(60, 21);
            this.筛选ToolStripMenuItem.Text = "指定IP/端口(&V)";
            // 
            // 筛选选项ToolStripMenuItem
            // 
            this.筛选选项ToolStripMenuItem.Name = "筛选选项ToolStripMenuItem";
            this.筛选选项ToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.筛选选项ToolStripMenuItem.Text = "指定IP/端口";
            this.筛选选项ToolStripMenuItem.Click += new System.EventHandler(this.筛选选项ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(61, 21);
            this.帮助ToolStripMenuItem.Text = "帮助(&H)";
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.关于ToolStripMenuItem.Text = "关于";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStrip_Open,
            this.ToolStrip_Save,
            this.toolStripSeparator1,
            this.ToolStrip_Start,
            this.ToolStrip_Stop,
            this.ToolStrip_Clear,
            this.toolStripSeparator2,
            this.ToolStrip_Filter,
            this.toolStripSeparator3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(804, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ToolStrip_Open
            // 
            this.ToolStrip_Open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStrip_Open.Image = ((System.Drawing.Image)(resources.GetObject("ToolStrip_Open.Image")));
            this.ToolStrip_Open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStrip_Open.Name = "ToolStrip_Open";
            this.ToolStrip_Open.Size = new System.Drawing.Size(23, 22);
            this.ToolStrip_Open.Text = "打开";
            this.ToolStrip_Open.ToolTipText = "打开";
            this.ToolStrip_Open.Click += new System.EventHandler(this.ToolStrip_Open_Click);
            // 
            // ToolStrip_Save
            // 
            this.ToolStrip_Save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStrip_Save.Image = ((System.Drawing.Image)(resources.GetObject("ToolStrip_Save.Image")));
            this.ToolStrip_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStrip_Save.Name = "ToolStrip_Save";
            this.ToolStrip_Save.Size = new System.Drawing.Size(23, 22);
            this.ToolStrip_Save.Text = "保存";
            this.ToolStrip_Save.Click += new System.EventHandler(this.ToolStrip_Save_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStrip_Start
            // 
            this.ToolStrip_Start.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStrip_Start.Image = ((System.Drawing.Image)(resources.GetObject("ToolStrip_Start.Image")));
            this.ToolStrip_Start.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStrip_Start.Name = "ToolStrip_Start";
            this.ToolStrip_Start.Size = new System.Drawing.Size(23, 22);
            this.ToolStrip_Start.Text = "开始";
            this.ToolStrip_Start.Click += new System.EventHandler(this.ToolStrip_Start_Click);
            // 
            // ToolStrip_Stop
            // 
            this.ToolStrip_Stop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStrip_Stop.Enabled = false;
            this.ToolStrip_Stop.Image = ((System.Drawing.Image)(resources.GetObject("ToolStrip_Stop.Image")));
            this.ToolStrip_Stop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStrip_Stop.Name = "ToolStrip_Stop";
            this.ToolStrip_Stop.Size = new System.Drawing.Size(23, 22);
            this.ToolStrip_Stop.Text = "停止";
            this.ToolStrip_Stop.Click += new System.EventHandler(this.ToolStrip_Stop_Click);
            // 
            // ToolStrip_Clear
            // 
            this.ToolStrip_Clear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStrip_Clear.Image = ((System.Drawing.Image)(resources.GetObject("ToolStrip_Clear.Image")));
            this.ToolStrip_Clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStrip_Clear.Name = "ToolStrip_Clear";
            this.ToolStrip_Clear.Size = new System.Drawing.Size(23, 22);
            this.ToolStrip_Clear.Text = "清除";
            this.ToolStrip_Clear.Click += new System.EventHandler(this.ToolStrip_Clear_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStrip_Filter
            // 
            this.ToolStrip_Filter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStrip_Filter.Image = ((System.Drawing.Image)(resources.GetObject("ToolStrip_Filter.Image")));
            this.ToolStrip_Filter.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ToolStrip_Filter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStrip_Filter.Name = "ToolStrip_Filter";
            this.ToolStrip_Filter.Size = new System.Drawing.Size(23, 22);
            this.ToolStrip_Filter.Text = "筛选";
            this.ToolStrip_Filter.Click += new System.EventHandler(this.ToolStrip_Filter_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 50);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listView_Data);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Controls.Add(this.statusStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(804, 379);
            this.splitContainer1.SplitterDistance = 198;
            this.splitContainer1.TabIndex = 2;
            // 
            // listView_Data
            // 
            this.listView_Data.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ID,
            this.Protocol,
            this.SourceIP,
            this.SourcePort,
            this.DestIP,
            this.DestPort,
            this.MessageBodyLen,
            this.MessageBodyTxt,
            this.IPHeaderHex,
            this.MessageHeaderHex,
            this.MessageBodyHex,
            this.IPHeaderLength ,
            this .MessageHeaderLen });
            this.listView_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_Data.FullRowSelect = true;
            this.listView_Data.HideSelection = false;
            this.listView_Data.LabelWrap = false;
            this.listView_Data.Location = new System.Drawing.Point(0, 0);
            this.listView_Data.Margin = new System.Windows.Forms.Padding(0);
            this.listView_Data.MultiSelect = false;
            this.listView_Data.Name = "listView_Data";
            this.listView_Data.ShowGroups = false;
            this.listView_Data.Size = new System.Drawing.Size(804, 198);
            this.listView_Data.TabIndex = 0;
            this.listView_Data.UseCompatibleStateImageBehavior = false;
            this.listView_Data.View = System.Windows.Forms.View.Details;
            this.listView_Data.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView_Data_ItemSelectionChanged);
            this.listView_Data.SelectedIndexChanged += new System.EventHandler(this.listView_Data_SelectedIndexChanged);
            // 
            // ID
            // 
            this.ID.Text = "ID";
            // 
            // Protocol
            // 
            this.Protocol.Text = "协议";
            // 
            // SourceIP
            // 
            this.SourceIP.Text = "源IP";
            this.SourceIP.Width = 120;
            // 
            // SourcePort
            // 
            this.SourcePort.Text = "源端口";
            // 
            // DestIP
            // 
            this.DestIP.Text = "目标IP";
            this.DestIP.Width = 120;
            // 
            // DestPort
            // 
            this.DestPort.Text = "目标端口";
            this.DestPort.Width = 110;
            // 
            // MessageBodyLen
            // 
            this.MessageBodyLen.Text = "数据（byte）";
            this.MessageBodyLen.Width = 110;
            // 
            // MessageBodyTxt
            // 
            this.MessageBodyTxt.Text = "数据包总字节数";
            this.MessageBodyTxt.Width = 200;
            // 
            // IPHeaderHex
            // 
            this.MessageHeaderLen.Text = "UDP/TCP/ICMP包头";
            this.MessageHeaderLen.Width = 120;
            this.IPHeaderLength.Text = "IP包头";
            this.IPHeaderLength.Width = 50;
            this.IPHeaderHex.Text = "IP报文头";
            this.IPHeaderHex.Width = 120;
            // 
            // MessageHeaderHex
            // 
            this.MessageHeaderHex.Text = "消息头";
            this.MessageHeaderHex.Width = 120;
            // 
            // MessageBodyHex
            // 
            this.MessageBodyHex.Text = "消息正文";
            this.MessageBodyHex.Width = 120;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.TextBox_Txt);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.TextBox_Hex);
            this.splitContainer2.Size = new System.Drawing.Size(804, 155);
            this.splitContainer2.SplitterDistance = 393;
            this.splitContainer2.TabIndex = 1;
            // 
            // TextBox_Txt
            // 
            this.TextBox_Txt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBox_Txt.Location = new System.Drawing.Point(0, 0);
            this.TextBox_Txt.Multiline = true;
            this.TextBox_Txt.Name = "TextBox_Txt";
            this.TextBox_Txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox_Txt.Size = new System.Drawing.Size(393, 155);
            this.TextBox_Txt.TabIndex = 0;
            // 
            // TextBox_Hex
            // 
            this.TextBox_Hex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextBox_Hex.Location = new System.Drawing.Point(0, 0);
            this.TextBox_Hex.Multiline = true;
            this.TextBox_Hex.Name = "TextBox_Hex";
            this.TextBox_Hex.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBox_Hex.Size = new System.Drawing.Size(407, 155);
            this.TextBox_Hex.TabIndex = 1;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ServiceStatus,
            this.PacketStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 155);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(804, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ServiceStatus
            // 
            this.ServiceStatus.AutoSize = false;
            this.ServiceStatus.Name = "ServiceStatus";
            this.ServiceStatus.Size = new System.Drawing.Size(60, 17);
            this.ServiceStatus.Text = "准备就绪";
            this.ServiceStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PacketStatus
            // 
            this.PacketStatus.AutoSize = false;
            this.PacketStatus.Name = "PacketStatus";
            this.PacketStatus.Size = new System.Drawing.Size(200, 17);
            this.PacketStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "xml";
            this.saveFileDialog1.Filter = "xml文档(*.xml)|*.xml|所有文件(*.*)|*.*";
            this.saveFileDialog1.Title = "保存";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "xml文档(*.xml)|*.xml|所有文件(*.*)|*.*";
            this.openFileDialog1.Title = "打开";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 429);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MiniSniffer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 监视ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 启动ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 停止ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 清空ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 筛选ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 筛选选项ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ToolStrip_Open;
        private System.Windows.Forms.ToolStripButton ToolStrip_Save;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ToolStrip_Start;
        private System.Windows.Forms.ToolStripButton ToolStrip_Stop;
        private System.Windows.Forms.ToolStripButton ToolStrip_Clear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton ToolStrip_Filter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox TextBox_Txt;
        private System.Windows.Forms.TextBox TextBox_Hex;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ServiceStatus;
        private System.Windows.Forms.ListView listView_Data;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Protocol;
        private System.Windows.Forms.ColumnHeader SourceIP;
        private System.Windows.Forms.ColumnHeader SourcePort;
        private System.Windows.Forms.ColumnHeader DestIP;
        private System.Windows.Forms.ColumnHeader DestPort;
        private System.Windows.Forms.ColumnHeader MessageBodyLen;
        private System.Windows.Forms.ColumnHeader IPHeaderHex;
        private System.Windows.Forms.ColumnHeader MessageBodyTxt;
        private System.Windows.Forms.ColumnHeader MessageHeaderHex;
        private System.Windows.Forms.ColumnHeader MessageBodyHex;
        private System.Windows.Forms.ColumnHeader IPHeaderLength;
        private System.Windows.Forms.ColumnHeader MessageHeaderLen;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripStatusLabel PacketStatus;
    }
}

