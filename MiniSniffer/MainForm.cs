using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MiniSniffer
{
    public partial class MainForm : Form
    {
        private int ItemID;
        private int FilterCount;
        private SnifferService Sniffer;
        private FilterOption FilterOptionForm;

        delegate void listViewDelegate(string ID, string Protocol, string SourceIP, string SourcePort, string DestIP, string DestPort, string MessageBodyLen, string MessageBodyTxt, string IPHeaderHex, string MessageHeaderHex, string MessageBodyHex,string IPHeaderLength,string MessageHeaderLen);

        const int ICMPDataOffset = 4;
        const int IGMPDataOffset = 4;
        const int TCPDataOffset = 20;
        const int UDPDataOffset = 8;

        public MainForm()
        {
            ItemID = 0;
            FilterCount = 0;
            Sniffer = new SnifferService();
            Sniffer.PacketArrival += new SnifferService.PacketArrivedEventHandler(Sniffer_PacketArrival);
            FilterOptionForm = new FilterOption();
            InitializeComponent();
        }

        private void ToolStrip_Open_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void ToolStrip_Save_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void ToolStrip_Start_Click(object sender, EventArgs e)
        {
            ToolStrip_Start.Enabled = false;
            Sniffer.Start();
            ToolStrip_Stop.Enabled = true;
            ServiceStatus.Text = "开始监听";
            UpdateStatus();
        }

        private void ToolStrip_Stop_Click(object sender, EventArgs e)
        {
            ToolStrip_Stop.Enabled = false;
            Sniffer.Stop();
            ToolStrip_Start.Enabled = true;
            ServiceStatus.Text = "停止监听";
            UpdateStatus();
        }

        private void ToolStrip_Clear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void ToolStrip_Filter_Click(object sender, EventArgs e)
        {
            FilterOptionForm.Show();
        }

        void Sniffer_PacketArrival(object sender, PacketArrivedEventArgs args)
        {
            Data_Receive(args.Protocol, args.OriginationAddress, args.OriginationPort, args.DestinationAddress, args.DestinationPort, args.IPHeaderLength, args.IPHeaderBuffer, args.MessageLength, args.MessageBuffer, args.PacketLength, args.PacketBuffer);
        }

        private void listView_Data_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            TextBox_Txt.Text = e.Item.SubItems[7].Text;
            TextBox_Hex.Text = e.Item.SubItems[10].Text;
        }

        public void Data_Receive(string Protocol, string SourceIP, string SourcePort, string DestIP, string DestPort, uint IPHeaderLength, Byte[] IPHeaderBuffer, uint MessageLength, Byte[] MessageBuffer, uint PacketLength, Byte[] PacketBuffer)
        {
            ItemID++;
            // 数据显示
            listViewDelegate listDelegate = new listViewDelegate(AddItem);
            int IPHeaderLen = (int)IPHeaderLength;
            string IPHeaderHex = GetDataHex(IPHeaderBuffer, 0, IPHeaderLen);

            int MessageHeaderLen = GetMessageHeaderLen(Protocol);
            string MessageHeaderHex = GetDataHex(MessageBuffer, 0, MessageHeaderLen);

            int MessageBodyLen = (int)MessageLength - MessageHeaderLen;
            string MessageBodyTxt = GetDataTxt(MessageBuffer, MessageHeaderLen, MessageBodyLen);
            string MessageBodyHex = GetDataHex(MessageBuffer, MessageHeaderLen, MessageBodyLen);
            int MessageALL = IPHeaderLen + MessageHeaderLen + MessageBodyLen;
            string IPHeader = Convert.ToString(IPHeaderLen);
            string MessageHLength = Convert.ToString(MessageHeaderLen);
            string AllLength= Convert.ToString(MessageALL);
            listView_Data.Invoke(listDelegate, ItemID.ToString(), Protocol, SourceIP, SourcePort, DestIP, DestPort, MessageBodyLen.ToString(), AllLength, IPHeaderHex, MessageHeaderHex, MessageBodyHex, IPHeader, MessageHLength);
        }


        private int GetMessageHeaderLen(string Protocol)
        {
            switch (Protocol)
            {
                case "ICMP": return ICMPDataOffset;
                case "IGMP": return IGMPDataOffset;
                case "TCP": return TCPDataOffset;
                case "UDP": return UDPDataOffset;
                case "UNKNOW": return 0;
                default: return 0;
            }
        }

        private string GetDataHex(Byte[] Data, int index, int count)
        {
            return System.Text.Encoding.Default.GetString(Data);
        }

        private string GetDataTxt(Byte[] Data, int index, int count)
        {
            Byte[] Temp = new Byte[Data.Length];

            Data.CopyTo(Temp, 0);

            for (int i = index; i < index + count; i++) 
            {
                if (Temp[i] == 0) Temp[i] = 46;
            }
            return System.Text.Encoding.Default.GetString(Temp, index, count);
        }

        private void AddItem(string ID, string Protocol, string SourceIP, string SourcePort, string DestIP, string DestPort, string MessageBodyLen, string MessageBodyTxt, string IPHeaderHex, string MessageHeaderHex, string MessageBodyHex,string IPHeaderLength, string MessageHeaderLen)
        {
            if (DataFilter(Protocol, SourceIP, SourcePort, DestIP, DestPort))
            {
                FilterCount++;
                listView_Data.Items.Add(new ListViewItem(new string[] { ID, Protocol, SourceIP, SourcePort, DestIP, DestPort, MessageBodyLen, MessageBodyTxt, IPHeaderHex, MessageHeaderHex, MessageBodyHex, IPHeaderLength, MessageHeaderLen }));
            }
            UpdateStatus();
        }

        private bool DataFilter(string Protocol, string SourceIP, string SourcePort, string DestIP, string DestPort)
        {
            if (FilterOptionForm.Protocol.IndexOf(Protocol) == -1) return false;
            if (SourceIP != FilterOptionForm.SourceIP && FilterOptionForm.SourceIP != "") return false;
            if (SourcePort != FilterOptionForm.SourcePort && FilterOptionForm.SourcePort != "") return false;
            if (DestIP != FilterOptionForm.DestIP && FilterOptionForm.DestIP != "") return false;
            if (DestPort != FilterOptionForm.DestPort && FilterOptionForm.DestPort != "") return false;

            return true;
        }

        private void Clear()
        {
            listView_Data.Items.Clear();
            ItemID = 0;
            FilterCount = 0;
            TextBox_Txt.Text = "";
            TextBox_Hex.Text = "";
            UpdateStatus();
        }

        private void UpdateStatus()
        {
            PacketStatus.Text = "收到数据包:" + ItemID.ToString() + ", 筛选:" + FilterCount.ToString();
        }

        private void Open()
        {
            openFileDialog1.ShowDialog();
            if (!System.String.IsNullOrEmpty(openFileDialog1.FileName))
            {
                System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
                xdoc.Load(openFileDialog1.FileName);

                foreach (System.Xml.XmlElement Data in xdoc.SelectNodes("SnifferData/Data"))
                {
                    string ID = Data.ChildNodes.Item(0).InnerText;
                    string Protocol = Data.ChildNodes.Item(1).InnerText;
                    string SourceIP = Data.ChildNodes.Item(2).InnerText;
                    string SourcePort = Data.ChildNodes.Item(3).InnerText;
                    string DestIP = Data.ChildNodes.Item(4).InnerText;
                    string DestPort = Data.ChildNodes.Item(5).InnerText;
                    string MessageBodyLen = Data.ChildNodes.Item(6).InnerText;
                    string MessageBodyTxt = Data.ChildNodes.Item(7).InnerText;
                    string IPHeaderHex = Data.ChildNodes.Item(8).InnerText;
                    string MessageHeaderHex = Data.ChildNodes.Item(9).InnerText;
                    string MessageBodyHex = Data.ChildNodes.Item(10).InnerText;
                    string IPHeaderLength = Data.ChildNodes.Item(10).InnerText;
                    string MessageHeaderLen = Data.ChildNodes.Item(10).InnerText;
                    AddItem(ID, Protocol, SourceIP, SourcePort, DestIP, DestPort, MessageBodyLen, MessageBodyTxt, IPHeaderHex, MessageHeaderHex, MessageBodyHex,IPHeaderLength ,MessageHeaderLen );
                }
            }
        }

        private void Save()
        {
            System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();
            System.Xml.XmlElement Root = xdoc.CreateElement("SnifferData");
            xdoc.AppendChild(Root);

            foreach (ListViewItem Item in listView_Data.Items)
            {
                System.Xml.XmlElement Data = xdoc.CreateElement("Data");
                Root.AppendChild(Data);

                System.Xml.XmlElement ID = xdoc.CreateElement("ID");
                ID.InnerText = Item.SubItems[0].Text;
                Data.AppendChild(ID);

                System.Xml.XmlElement Protocol = xdoc.CreateElement("Protocol");
                Protocol.InnerText = Item.SubItems[1].Text;
                Data.AppendChild(Protocol);

                System.Xml.XmlElement SourceIP = xdoc.CreateElement("SourceIP");
                SourceIP.InnerText = Item.SubItems[2].Text;
                Data.AppendChild(SourceIP);

                System.Xml.XmlElement SourcePort = xdoc.CreateElement("SourcePort");
                SourcePort.InnerText = Item.SubItems[3].Text;
                Data.AppendChild(SourcePort);

                System.Xml.XmlElement DestIP = xdoc.CreateElement("DestIP");
                DestIP.InnerText = Item.SubItems[4].Text;
                Data.AppendChild(DestIP);

                System.Xml.XmlElement DestPort = xdoc.CreateElement("DestPort");
                DestPort.InnerText = Item.SubItems[5].Text;
                Data.AppendChild(DestPort);

                System.Xml.XmlElement MessageBodyLen = xdoc.CreateElement("MessageBodyLen");
                MessageBodyLen.InnerText = Item.SubItems[6].Text;
                Data.AppendChild(MessageBodyLen);

                System.Xml.XmlElement MessageBodyTxt = xdoc.CreateElement("MessageBodyTxt");
                MessageBodyTxt.InnerText = Item.SubItems[7].Text;
                Data.AppendChild(MessageBodyTxt);

                System.Xml.XmlElement IPHeaderHex = xdoc.CreateElement("IPHeaderHex");
                IPHeaderHex.InnerText = Item.SubItems[8].Text;
                Data.AppendChild(IPHeaderHex);

                System.Xml.XmlElement MessageHeaderHex = xdoc.CreateElement("MessageHeaderHex");
                MessageHeaderHex.InnerText = Item.SubItems[9].Text;
                Data.AppendChild(MessageHeaderHex);

                System.Xml.XmlElement MessageBodyHex = xdoc.CreateElement("MessageBodyHex");
                MessageBodyHex.InnerText = Item.SubItems[10].Text;
                Data.AppendChild(MessageBodyHex);

                System.Xml.XmlElement IPHeaderLength = xdoc.CreateElement("MessageBodyHex");
                MessageBodyHex.InnerText = Item.SubItems[10].Text;
                Data.AppendChild(MessageBodyHex);

                System.Xml.XmlElement MessageHeaderLen = xdoc.CreateElement("MessageBodyHex");
                MessageBodyHex.InnerText = Item.SubItems[10].Text;
                Data.AppendChild(MessageBodyHex);
            }

            saveFileDialog1.ShowDialog();
            if (!System.String.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                string XmlFileName = saveFileDialog1.FileName;
                xdoc.Save(XmlFileName);
            }

        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 启动ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sniffer.Start();
            ServiceStatus.Text = "开始监听";
            UpdateStatus();
        }

        private void 停止ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sniffer.Stop();
            ServiceStatus.Text = "停止监听";
            UpdateStatus();
        }

        private void 清空ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void 筛选选项ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FilterOptionForm.Show();
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox AboutForm = new AboutBox();
            AboutForm.Show();
        }

        private void listView_Data_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}