using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace MiniSniffer
{
    class SnifferService
    {
        private MyTryRaw[] Sniffers;

        public SnifferService()
        {
            string[] IPList = GetLocalIPList();
            Sniffers = new MyTryRaw[IPList.Length];
            for (int i = 0; i < IPList.Length; i++)
            {
                try
                {
                    Sniffers[i] = new MyTryRaw();
                    try
                    {

                        Sniffers[i].BindSocket(IPList[i]);
                    }
                    catch { }

                    try
                    {
                        Sniffers[i].SetOption();
                    }
                    catch { }

                    Sniffers[i].PacketArrival += new MyTryRaw.PacketArrivedEventHandler(SnifferServer_PacketArrival);
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show("������" + IPList[i] + "�ϵļ�������ʧ��:" + ex.Message);
                }
            }

        }

        #region   Events
        public delegate void PacketArrivedEventHandler(Object sender, PacketArrivedEventArgs args);

        public event PacketArrivedEventHandler PacketArrival;
        #endregion


        void SnifferServer_PacketArrival(object sender, PacketArrivedEventArgs args)
        {
            if (PacketArrival != null)
            {
                PacketArrival(this, args);
            }
        }


        public void Start()
        {
            foreach (MyTryRaw Sniffer in Sniffers)
            {
                Sniffer.Start();
            }
        }

        public void Stop()
        {
            foreach (MyTryRaw Sniffer in Sniffers)
            {
                Sniffer.Stop();
            }
        }

        private string[] GetLocalIPList()
        {
            string HostName = Dns.GetHostName();
            IPHostEntry IPEntry = Dns.GetHostEntry(HostName);
            IPAddress[] IPList = IPEntry.AddressList;

            System.Collections.ArrayList LocalIPList = new System.Collections.ArrayList();
            for (int i = 0; i < IPList.Length; i++)
            {
                if (IPList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    LocalIPList.Add(IPList[i].ToString());
                }
            }
            return (string[])LocalIPList.ToArray(typeof(string));
        }
    }
}
