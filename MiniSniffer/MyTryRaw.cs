using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;


public class PacketArrivedEventArgs : System.EventArgs
{
    public PacketArrivedEventArgs()
    {
    }
    public string Protocol = String.Empty;
    public string IPVersion = String.Empty;
    public string OriginationAddress = String.Empty;
    public string OriginationPort = String.Empty;
    public string DestinationAddress = String.Empty;
    public string DestinationPort = String.Empty;
    public uint PacketLength = 0;
    public uint IPHeaderLength = 0;
    public uint MessageLength = 0;
    public byte[] PacketBuffer = null;
    public byte[] IPHeaderBuffer = null;
    public byte[] MessageBuffer = null;

}

///   <summary>   
///   MyTryRaw   的摘要说明。   
///   </summary>   
public class MyTryRaw
{
    #region   Attributes

    private static int len_receive_buf;   //得到的数据流的长度   
    private byte[] receive_buf_bytes = null;                     //收到的字节   
    private Socket socket = null;               //声明套接字   
    private const int SIO_RCVALL = unchecked((int)0x98000001);//监听所有的数据包   
    public static bool isStop = true;

    public Socket m_socket
    {
        get
        {
            return socket;
        }
    }

    #endregion

    #region   Events
    public delegate void PacketArrivedEventHandler(Object sender, PacketArrivedEventArgs args);

    public event PacketArrivedEventHandler PacketArrival;
    #endregion

    #region   OnEvents
    protected virtual void OnPacketArrival(PacketArrivedEventArgs e)
    {
        if (PacketArrival != null)
        {
            PacketArrival(this, e);
        }

    }

    #endregion

    #region   Constructor
    public MyTryRaw()
    {  
        len_receive_buf = 4096;
        receive_buf_bytes = new byte[len_receive_buf];
    }
    #endregion

    #region   Functions

    public void BindSocket(string IP)
    {
        IPAddress ipAddress = IPAddress.Parse(IP);
        this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.IP);
        try
        {
            socket.Blocking = false;
            socket.Bind(new IPEndPoint(ipAddress, 0));
        }
        catch (Exception E)
        {
            throw (E);
        }
    }

    public void SetOption()
    {
        try
        {
            socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.HeaderIncluded, 1);
            byte[] IN = new byte[4] { 1, 0, 0, 0 };
            byte[] OUT = new byte[4];
            int ret_code = -1;
            ret_code = socket.IOControl(SIO_RCVALL, IN, OUT);
            ret_code = OUT[0] + OUT[1] + OUT[2] + OUT[3];
        }
        catch (Exception E)
        {
            throw (E);
        }
    }

    public void Start()
    {
        isStop = false;
        BeginReceive();
    }

    public void Stop()
    {
        isStop = true;
    }

    public void ShutDown()
    {
        isStop = true;
        if (socket != null)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
        socket = null;
    }

    private void BeginReceive()
    {
        isStop = false;
        if (socket != null)
        {
            object state = null;
            state = socket;
            socket.BeginReceive(receive_buf_bytes, 0, receive_buf_bytes.Length, SocketFlags.None, new AsyncCallback(CallReceive), state);
        }
    }

    private void CallReceive(IAsyncResult ar)//异步回调   
    {
        int received_bytes = 0;
        Socket m_socket = (Socket)ar.AsyncState;
        if (m_socket != null)
        {
            if (isStop == false)
            {
                received_bytes = socket.EndReceive(ar);

                Receive(receive_buf_bytes, received_bytes);
            }
            if (isStop == false)
            {
                BeginReceive();
            }
        }
    }

    private void Receive(byte[] receivedBytes, int receivedLength)
    {
        PacketArrivedEventArgs e = new PacketArrivedEventArgs();

        int IPVersion = Convert.ToInt16((receivedBytes[0] & 0xF0) >> 4);
        e.IPVersion = IPVersion.ToString();

        e.IPHeaderLength = Convert.ToUInt32((receivedBytes[0] & 0x0F) << 2);

        if (receivedBytes.Length >= 20)
        {
            switch (Convert.ToInt16(receivedBytes[9]))
            {
                case 1:
                    e.Protocol = "ICMP";
                    break;
                case 2:
                    e.Protocol = "IGMP";
                    break;
                case 6:
                    e.Protocol = "TCP";
                    break;
                case 17:
                    e.Protocol = "UDP";
                    break;
                default:
                    e.Protocol = "UNKNOW";
                    break;
            }

            e.OriginationAddress = Convert.ToInt16(receivedBytes[12]).ToString() + "." + Convert.ToInt16(receivedBytes[13]).ToString() + "." + Convert.ToInt16(receivedBytes[14]).ToString() + "." + Convert.ToInt16(receivedBytes[15]).ToString();
            e.DestinationAddress = Convert.ToInt16(receivedBytes[16]).ToString() + "." + Convert.ToInt16(receivedBytes[17]).ToString() + "." + Convert.ToInt16(receivedBytes[18]).ToString() + "." + Convert.ToInt16(receivedBytes[19]).ToString();

            int Oport = ((receivedBytes[20] << 8) + receivedBytes[21]);
            e.OriginationPort = Oport.ToString();

            int Dport = ((receivedBytes[22] << 8) + receivedBytes[23]);
            e.DestinationPort = Dport.ToString();

            e.PacketLength = (uint)receivedLength;

            e.MessageLength = e.PacketLength - e.IPHeaderLength;

            e.PacketBuffer = new byte[e.PacketLength];
            e.IPHeaderBuffer = new byte[e.IPHeaderLength];
            e.MessageBuffer = new byte[e.MessageLength];

            Array.Copy(receivedBytes, 0, e.PacketBuffer, 0, (int)e.PacketLength);

            Array.Copy(receivedBytes, 0, e.IPHeaderBuffer, 0, e.IPHeaderLength);

            Array.Copy(receivedBytes, e.IPHeaderLength, e.MessageBuffer, 0, (int)e.MessageLength);
        }

        OnPacketArrival(e);
    }

    #endregion
}

